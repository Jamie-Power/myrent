package controllers;

import play.*;
import play.mvc.*;
import utils.Geodistance;
import utils.LatLng;

import java.util.*;

import models.*;

public class Landlords extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void login()
  {
    render();
  }
  
  public static void editprofile()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Landlords.login();
    }
  }

/*  public static void logout()
  {
    session.clear();
    index();
  }*/
  
  public static void index()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Landlords.login();
    }
  }
  
  public static void config()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
	  Landlord currentLandlord = getCurrentUser();
	  List<Residence> residences = currentLandlord.residences;
      render(residences);
    }
    else
    {
      Landlords.login();
    }
  }
  
  
 // public static void register(boolean conditions, String firstName, String lastName, String email, String password)
  public static void register(Landlord landlord)
  {
    Logger.info(landlord.conditions + " " + landlord.firstName + " " + landlord.lastName + " " + landlord.email + " "  + landlord.password);
    
   // Landlord landlord = new Landlord (conditions, firstName, lastName, email, password);
    landlord.save();
    login();
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    Landlord landlord = Landlord.findByEmail(email);
    if ((landlord != null) && (landlord.checkPassword(password) == true))
    {
        Logger.info("Authentication successful");
        session.put("logged_in_userid", landlord.id);
        session.put("logged_in_user_type", "landlord");
        session.put("logged_status", "logged_in");
        Home.index();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
  
  public static Landlord getCurrentUser()
  {
    Landlord landlord = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      landlord = Landlord.findById(Long.parseLong(userId));
    }
    return landlord;
  }
  
  public static void logout()
  {
	  if(session.get("logged_in_user_type") == "tenant"){
		  Tenants.setSessionLogout();
	  }
	  else{
    setSessionLogout();
    Welcome.index();
  }
  }
  
  /*
   * clear session on logout
   */
  protected static void setSessionLogout()
  {
    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      session.clear();
      session.put("logged_status", "logged_out");
    }
  }
  
	public static void editDetails(String firstName, String lastName, String email, String password)
	{

		String userId = session.get("logged_in_userid");
		Landlord landlord = Landlord.findById(Long.parseLong(userId));
		//Landlord landlord = getCurrentUser();
		landlord.firstName = firstName;
		landlord.lastName = lastName;
		landlord.email = email;
		landlord.password = password;
		landlord.save();
		Logger.info("Details changed to: " + firstName + lastName + email + password);
		Landlords.config();
	}
}