package controllers;

import play.*;
import play.mvc.*;
import utils.Geodistance;
import utils.LatLng;

import java.util.*;

import models.*;

public class Administrators extends Controller
{
	
	  public static void index()
	  {
		  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
	    {
	      List<Landlord> landlords = Landlord.findAll();
	      List<Tenant> tenants = Tenant.findAll();

	      render(landlords, tenants);
	    }
	    else
	    {
	      Administrators.login();
	    }
	  }
	  public static void login()
	  {
	    render();
	  }
	  
	  public static void authenticate(String email, String password)
	  {
	    Logger.info("Attempting to authenticate with " + email + ":" +  password);

	    Administrator administrator = Administrator.findByEmail(email);
	    if ((administrator != null) && (administrator.checkPassword(password) == true))
	    {
	        Logger.info("Authentication successful");
	        session.put("logged_in_user_type", "landlord");
	        session.put("logged_status", "logged_in");
	        Administrators.index();
	    }
	    else
	    {
	      Logger.info("Authentication failed");
	      login();  
	    }
	  }
	  
	  protected static void setSessionLogout()
	  {
	    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
	    {
	      session.clear();
	      session.put("logged_status", "logged_out");
	    }
	  }
	  
	  public static void logout()
	  {
		  if(session.get("logged_in_user_type") == "administrator"){
			  Administrators.setSessionLogout();
		  }
		  else{
	    setSessionLogout();
	    Welcome.index();
	    }
      }
	  
	  public static void lsignup()
	  {
		  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
	    {
	      render();
	    }
	    else
	    {
	      Administrators.login();
	    }
	  }
	  
	  public static void tsignup()
	  {
		  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
	    {
	      render();
	    }
	    else
	    {
	      Administrators.login();
	    }
	  }
	  
	  public static void home()
	  {
		  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
	    {
	      render();
	    }
	    else
	    {
	      Administrators.login();
	    }
	  }
	  
	  public static void lregister(Landlord landlord)
	  {
	    Logger.info(landlord.firstName + " " + landlord.lastName + " " + landlord.email + " "  + landlord.password);
	    landlord.save();
	    Administrators.index();
	  }
	  
	  public static void tregister(Tenant tenant)
	  {
	    Logger.info(tenant.firstName + " " + tenant.lastName + " " + tenant.email + " "  + tenant.password);
	    tenant.save();
	    Administrators.index();
	  }
	  
	   public static void deltenant(String email) 
	    {
	    	Tenant tenant = Tenant.findByEmail(email);
	    	if(tenant != null) 
	    	{
	    	  tenant.delete();
	    	}
	    	Administrators.index();
	    }      
	    
	    public static void dellandlord(String email) 
	    {
	    	Landlord landlord = Landlord.findByEmail(email);
	    	if(landlord != null) 
	    	{
	    	  landlord.delete();
	    	}
	    	Administrators.index();
	    }   
}
