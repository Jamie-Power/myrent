package controllers;

import play.mvc.Controller;

public class Acknowledgement extends Controller
{
  public static void index()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Landlords.login();
    }
  }
}