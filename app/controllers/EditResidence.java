package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

import java.util.ArrayList;
import java.util.List;

import play.mvc.Before;
import play.mvc.Controller;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

public class EditResidence extends Controller {

    public static void index(long id) 
    {
    	
    	Residence residence = Residence.findById(id);
        if(residence != null)
        {
          render(residence);	
        }
        else{
        	Landlords.config();
        }
    }
    
    public static void edit(int rent, long id)
    {
    	Residence residence = Residence.findById(id);
    	//residence.eircode = eircode;
    	residence.rent = rent;
    	
    	residence.save();
    	
    	Landlords.config();
    }
    
    public static void deleteres(long id) 
    {
    	Residence residence = Residence.findById(id);
    	if(residence != null) 
    	{
    	  residence.delete();
    	}
    	Landlords.config();
    }    

}