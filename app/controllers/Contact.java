package controllers;

import play.Logger;
import play.mvc.Controller;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import models.Landlord;

/**
 * This class configured only for localhost
 * 
 * @author jfitzgerald
 * 
 */
public class Contact extends Controller
{
  public static void index()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Welcome.index();
    }
  }

  /**
   * 
   * @param firstName
   *          : the message sender's first and
   * @param lastName
   *          : last name.
   * @param emailSender
   *          : the email address of the sender of the message.
   * @param messageTxd
   *          : the message.
   */
  //public static void sendMessage(String firstName, String lastName, String emailSender, String messageTxd)
  public static void sendMessage(Landlord user, String messageTxd)
 {

    if (Landlords.getCurrentUser() == null)
    {
      Landlords.login();
    }
    final String username = "xxxxxx@yahoo.com";
    final String password = "password";

    Properties props = new Properties();
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.smtp.host", "smtp.mail.yahoo.com");
    props.put("mail.smtp.port", "587");

    Session session = Session.getInstance(props, new javax.mail.Authenticator()
    {
      protected PasswordAuthentication getPasswordAuthentication()
      {
        return new PasswordAuthentication(username, password);
      }
    });

    try
    {
      String forwarderAddress = username;
      String destinationAddress1 = user.email;
      String destinationAddress2 = username;
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(forwarderAddress));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinationAddress1));
      message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(destinationAddress2));
      message.setSubject("Message for MyRent Webmaster");
      message.setText(messageTxd);

      Transport.send(message);

    }
    catch (MessagingException e)
    {
      Logger.info(e.getMessage());
      Acknowledgement.index();// a temporary fix: an exception caught when
                              // sendMessage invoked from Cloud
      throw new RuntimeException(e);
    }

    Acknowledgement.index();
  }
}