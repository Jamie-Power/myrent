package controllers;

import play.*;
import play.mvc.*;
import utils.Circle;
import utils.Geodistance;
import utils.LatLng;

import java.util.*;

import org.json.simple.JSONObject;

import models.*;

public class Tenants extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void login()
  {
    render();
  }
  
  public static void editprofile()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Tenants.login();
    }
  }
  
  public static void index()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in") && session.get("logged_in_user_type").equals("tenant"))
    {
	  Tenant tenant = getCurrentUser();
	  Residence residence = tenant.residence;
	  List<Residence> residences = new ArrayList<Residence>();	   
	  List<Residence> residencesAll = Residence.findAll();
	    for (Residence res : residencesAll)
	    {
	      
	      if (res.tenant == null)
	      {
	        residences.add(res);
	      }
	    }
      render(residence, residences);
    }
    else
    {
      Tenants.login();
    }
  }
  
  public static void home()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Tenants.login();
    }
  }
  
  public static void register(Tenant tenant)
  {
    Logger.info(tenant.firstName + " " + tenant.lastName + " " + tenant.email + " "  + tenant.password);
    
   // User user = new User (firstName, lastName, email, password);
    tenant.save();
    
    Tenants.login();
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    Tenant tenant = Tenant.findByEmail(email);
    if ((tenant != null) && (tenant.checkPassword(password) == true))
    {
        Logger.info("Authentication successful");
        session.put("logged_in_userid", tenant.id);
        session.put("logged_in_user_type","tenant");
        session.put("logged_status", "logged_in");
        Tenants.home();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
  
  public static Tenant getCurrentUser()
  {
    Tenant tenant = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      tenant = Tenant.findById(Long.parseLong(userId));
    }
    return tenant;
  }
  
  public static void logout()
  {
	  if(session.get("logged_in_user_type") == "tenant"){
		  Tenants.setSessionLogout();
	  }
	  else{
    setSessionLogout();
    Welcome.index();
    }
  }
  /*
   * clear session on logout
   */
  protected static void setSessionLogout()
  {
    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      session.clear();
      session.put("logged_status", "logged_out");
    }
  }
  
	public static void editDetails(String firstName, String lastName, String email, String password)
	{

		String userId = session.get("logged_in_userid");
		Tenant tenant = Tenant.findById(Long.parseLong(userId));
		tenant.firstName = firstName;
		tenant.lastName = lastName;
		tenant.email = email;
		tenant.password = password;
		tenant.save();
		Logger.info("Details changed to: " + firstName + lastName + email + password);
		Home.index();
	}
	
	public static void terminateTenancy()
	{
		String userId = session.get("logged_in_userid");
		Tenant tenant = Tenant.findById(Long.parseLong(userId));
		Residence residence = tenant.residence;
		residence.tenant = null;
		residence.save();
		tenant.residence = null;
		tenant.save();
		
		JSONObject obj = new JSONObject();
	    String value = "You have successfully terminated your residency.";
	    obj.put("retVal", value);
	    renderJSON(obj);
		
	}
	
	public static void rentResidence(long residenceId)
	{
		String userId = session.get("logged_in_userid");
		Tenant tenant = Tenant.findById(Long.parseLong(userId));
		Residence residence = Residence.findById(residenceId);
		
		tenant.residence = residence;
		tenant.save();
		
		residence.tenant = tenant;
		residence.save();
		
		JSONObject obj = new JSONObject();
	    String value = "You have successfully rented "+residence.eircode+".";
	    obj.put("retVal", value);
	    obj.put("eircode", residence.eircode);
	    renderJSON(obj);
	}
	
	  public static void generateReport(double radius, double latcenter, double lngcenter)
	  {
	    // All reported residences will fall within this circle
	    Circle circle = new Circle(latcenter, lngcenter, radius);
	    Landlord landlord = Landlords.getCurrentUser();
	    List<Residence> residences = new ArrayList<Residence>();
	    // Fetch all residences and filter out those within circle
	    List<Residence> residencesAll = Residence.findAll();
	    for (Residence res : residencesAll)
	    {
	      LatLng residenceLocation = res.getGeolocation();
	      if (Geodistance.inCircle(residenceLocation, circle) && res.tenant == null)
	      {
	        residences.add(res);
	      }
	    }
	    render("ReportController/renderReport.html", landlord, circle, residences);
	  }
}