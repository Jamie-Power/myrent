
package controllers;

import java.util.Date;

import models.Residence;
import models.Landlord;
import play.Logger;
import play.mvc.Controller;
import org.json.simple.JSONObject;

public class InputData extends Controller
{

  /**
   * Render data input view (logged-in users only).
   */
  public static void index()
  {
	  if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Welcome.index();
    }
  }

  /**
   * Update models with residential data
   */
  public static void dataCapture(Residence residence)
  {
    Landlord landlord = Landlords.getCurrentUser();
    residence.addLandlord(landlord);   
    residence.dateRegistered = new Date();
    residence.save();

    Logger.info("Residence data received and saved");
    Logger.info("Residence type: " + residence.residenceType);

    //index();
    JSONObject obj = new JSONObject();
    String value = "Congratulations. You have successfully registered your " + residence.residenceType +".";
    obj.put("inputdata", value);
    renderJSON(obj);
  }

}