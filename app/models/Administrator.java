package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Administrator extends Model
{
	
	  public String email;
	  public String password;
	  
	  public Administrator(String email, int age, String password)
	  {
	    this.email     = email;
	    this.password  = password;
	  }
	  
	  public boolean checkPassword(String password)
	  {
	    return this.password.equals(password);
	  }  
	  
	  public static Administrator findByEmail(String email)
	  {
	    return find("email", email).first();
	  }
}
