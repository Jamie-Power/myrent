package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Tenant extends Model
{
  @OneToOne
  public Residence residence;
  
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  
  public Tenant(String firstName, String lastName, String email, int age, String password)
  {
    this.firstName = firstName;
    this.lastName  = lastName;
    this.email     = email;
    this.password  = password;
  }
  
  public static Tenant findByEmail(String email)
  {
    return find("email", email).first();
  }

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }  
  
  public String toString()
  {
    return firstName + " " + lastName;
  }
  
}