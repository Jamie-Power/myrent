package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Landlord extends Model
{
  @OneToMany(mappedBy="landlord", cascade=CascadeType.ALL)
  public List<Residence> residences = new ArrayList<Residence>();
  public boolean conditions;
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  
  public Landlord(boolean conditions, String firstName, String lastName, String email, int age, String password)
  {
	this.conditions = conditions;
    this.firstName = firstName;
    this.lastName  = lastName;
    this.email     = email;
    this.password  = password;
  }
  
  public static Landlord findByEmail(String email)
  {
    return find("email", email).first();
  }

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }  
  
  public String toString()
  {
    return firstName + " " + lastName;
  }
  
}