package models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import play.db.jpa.Model;
import utils.LatLng;

@Entity
public class Residence extends Model
{
  @ManyToOne
  public Landlord landlord;
  
  @OneToOne(mappedBy = "residence")
  public Tenant tenant;
  
  public String geolocation; 
  public Date dateRegistered;
  public int rent;
  public int numberBedrooms;
  public int numberBathrooms;
  public int area;
  public String residenceType;
  public String eircode;

  public void addLandlord(Landlord landlord)
  {
    this.landlord = landlord;
    this.save();
  }
  
  public void addTenant(Tenant tenant)
  {
	  this.tenant = tenant;
	  this.save();
  }

  public LatLng getGeolocation()
  {
    return LatLng.toLatLng(this.geolocation);
  }
}