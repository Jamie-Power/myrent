$( document ).ready(function() {
	var currentResidency = $('#currentResidenceId').val()
	if(!currentResidency){
		$('#terminateResidency').prop('disabled', true)
		$('#rentResidency').prop('disabled', false)
	}else {
		$('#terminateResidency').prop('disabled', false)
		$('#rentResidency').prop('disabled', true)
	}
})

$('#terminateResidency').on('click', function(){
	var currentResidence = $('#currentResidenceId').val()
	if(!currentResidence){
		alert('You are not currently renting a property.')
		return
	}
	$.ajax({
        type: 'POST',
        url: '/tenants/terminateTenancy',
        success: function(response) { 
            $('#currentResidenceId').val('');
            $('#currentResidenceEircode').val('');
            $('#terminateResidency').prop('disabled', true)
            $('#rentResidency').prop('disabled', false)
            alert(response.retVal)
        }
      });
})

$('#rentResidency').on('click', function(){
	var newResidence = $('#newResidenceId').val()
	if(!newResidence){
		alert('Please select a property to rent.')
		return
	}
	$.ajax({
        type: 'POST',
        url: '/tenants/rentResidence',
        data: {residenceId: newResidence},
        success: function(response) { 
            $('#currentResidenceId').val(newResidence);
            $('#currentResidenceEircode').val(response.eircode);
            $('#terminateResidency').prop('disabled', false)
            $('#rentResidency').prop('disabled', true)
            alert(response.retVal)
        }
      });
})